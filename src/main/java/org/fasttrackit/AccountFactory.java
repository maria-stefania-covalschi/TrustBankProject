package org.fasttrackit;

public class AccountFactory {
    public static final String RON = "1";
    public static final String EUR = "2";
    public static final String USD = "3";
    public static final String GBT = "4";

    static Account createAccount(String userOption) {
        Account account = null;
        switch (userOption) {
            case RON: {
                account = new Account("RO05BTRLRONCRT0099990506", "RON");
                break;
            }
            case EUR: {
                account = new Account("RO05BTRLEURCRT0099990506", "EUR");
                break;
            }
            case USD: {
                account = new Account("RO05BTRLUSDCRT0099990506", "USD");
                break;
            }
            case GBT: {
                account = new Account("RO05BTRLGBTCRT0099990506", "GBT");
                break;
            }
            default:
                System.out.println("Sorry, unknown option.");
        }
        return account;
    }
}
