package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

// (user name/password/Name/Surname/email/Phone)
public class User {
    private final Credentials credentials;
    private final Account defaultAccount;
    private final List<Account> accounts;
    private final String name;
    private final String surname;
    private String email;
    private String phone;

    public User(String loginId, String password, String name, String surname) {
        this.credentials = new Credentials(loginId, password);
        this.defaultAccount = new Account("RO05BTRLRONCRT0099990505", "RON");
        this.accounts = new ArrayList<>();
        this.name = name;
        this.surname = surname;
        this.email = "";
        this.phone = "";
    }

    public void registerAccount(Account newAccount) {

        this.accounts.add(newAccount);
    }

    public boolean isAuthenticated(Credentials credentials) {

        return this.credentials.equals(credentials);
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public Account getDefaultAccount() {
        return defaultAccount;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }


    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
